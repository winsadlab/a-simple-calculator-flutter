import 'package:a_simple_calculator/recent_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({Key? key}) : super(key: key);

  @override
  State<CalculatorScreen> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> with TickerProviderStateMixin {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final double padding = 16;
  String calculateValue = '';
  String answer = '';

  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: 500),
    vsync: this,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('A Sample Calculator'),
        centerTitle: true,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const RecentScreen(),
                ),
              ).then((value) => _onRecent(value));
            },
            icon: const Icon(Icons.replay_circle_filled_outlined),
            tooltip: 'Recents Calculate',
          ),
        ],
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              AnimatedBuilder(
                animation: _controller,
                builder: (context, child) {
                  return Column(
                    children: [
                      Text(
                        calculateValue.isEmpty ? '0' : calculateValue,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: answer.isNotEmpty ? 20 : 30),
                      ),
                      if (answer.isNotEmpty)
                        Text(
                          answer,
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 40),
                        ),
                    ],
                  );
                },
              ),
              SizedBox(height: padding * 2),
              // First Row
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyButton(
                    onTap: (value) {
                      setState(() {
                        calculateValue = '';
                        answer = '';
                      });
                    },
                    value: 'CA',
                  ),
                  MyButton(
                    onTap: (value) {
                      setState(() {
                        answer = '';
                      });
                    },
                    value: 'C',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: ' % ',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: ' / ',
                  ),
                ],
              ),
              SizedBox(height: padding / 4),

              // Second Row
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '1',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '2',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '3',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: ' X ',
                  ),
                ],
              ),
              SizedBox(height: padding / 4),

              // Third Row
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '4',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '5',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '6',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: ' - ',
                  ),
                ],
              ),
              SizedBox(height: padding / 4),

              // Fourth Row
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '7',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '8',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '9',
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: ' + ',
                  ),
                ],
              ),
              SizedBox(height: padding / 4),

              // Fifth Row
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '0',
                    isZeroButton: true,
                  ),
                  MyButton(
                    onTap: _onTapValueButton,
                    value: '.',
                  ),
                  MyButton(
                    onTap: (value) {
                      _onTapEqual();
                    },
                    value: '=',
                  ),
                ],
              ),
              SizedBox(height: padding / 2),
            ],
          ),
        ),
      ),
    );
  }

  // on tap from recent page
  void _onRecent(dynamic value) {
    if (value != null) {
      setState(() {
        calculateValue = value;
      });

      _onTapEqual();
    }
  }

  void _onTapValueButton(String value) {
    setState(() {
      calculateValue += value;

      if (answer.isNotEmpty) answer = '';
    });
  }

  // function to calculate the input operation
  void _onTapEqual() {
    try {
      String finaluserinput = calculateValue;
      finaluserinput = calculateValue.replaceAll('X', '*');

      Parser p = Parser();
      Expression exp = p.parse(finaluserinput);
      ContextModel cm = ContextModel();
      double eval = exp.evaluate(EvaluationType.REAL, cm);
      setState(() {
        answer = eval.toString();
      });

      _addToRecent();
    } catch (error) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('You Enter Wrong Input')));
    }
  }

  Future<void> _addToRecent() async {
    try {
      final String value = calculateValue + '= ' + answer;
      SharedPreferences prefs = await _prefs;

      if (!recentLsit.contains(value)) {
        recentLsit.add(value);
        await prefs.setStringList(key, recentLsit);
      }
    } catch (error) {
      // print(error);
    }
  }
}

class MyButton extends StatelessWidget {
  final String value;
  final void Function(String value) onTap;
  final bool isZeroButton;

  const MyButton({
    Key? key,
    required this.value,
    required this.onTap,
    this.isZeroButton = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      width: isZeroButton ? 160 : 80,
      padding: const EdgeInsets.all(5),
      child: TextButton(
        onPressed: () => onTap(value),
        style: TextButton.styleFrom(
          elevation: 3,
          padding: const EdgeInsets.symmetric(vertical: 10),
          primary: Colors.grey,
          backgroundColor: Colors.white,
        ),
        child: Text(
          value,
          style: const TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            // color: Colors.black,
          ),
        ),
      ),
    );
  }
}
