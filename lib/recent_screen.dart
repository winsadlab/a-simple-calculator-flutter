import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<String> recentLsit = [];
const String key = 'recents';

class RecentScreen extends StatefulWidget {
  const RecentScreen({Key? key}) : super(key: key);

  @override
  State<RecentScreen> createState() => _RecentScreenState();
}

class _RecentScreenState extends State<RecentScreen> {
  List<String> recents = [];

  @override
  void initState() {
    onPrefInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Recents'),
        centerTitle: true,
        elevation: 0,
      ),
      body: SafeArea(
        child: Builder(builder: (context) {
          if (recents.isEmpty) {
            return const Center(
              child: Text('Recent is Empty'),
            );
          }

          return ListView.separated(
            itemCount: recents.length,
            itemBuilder: (context, index) {
              final String recent = recents[index];

              final String title = recent.substring(0, recent.indexOf('='));
              final String subTitle = recent.substring(recent.indexOf('='), recent.length);

              return ListTile(
                title: Text(title),
                subtitle: Text(
                  subTitle,
                  style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                onTap: () {
                  Navigator.pop(context, title.trim());
                },
                trailing: IconButton(
                  onPressed: () => _onremoveOnce(index),
                  icon: Icon(
                    Icons.clear,
                    color: Colors.grey.withOpacity(0.5),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) => Divider(color: Colors.blue.withOpacity(0.5)),
          );
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onRemovedAll,
        tooltip: 'Removed All',
        child: const Icon(Icons.delete),
      ),
    );
  }

  Future<void> onPrefInit() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    recentLsit = prefs.getStringList(key) ?? [];

    setState(() {
      recents = recentLsit;
    });
  }

  Future<void> _onremoveOnce(int index) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    recentLsit.removeAt(index);

    await prefs.setStringList(key, recentLsit);

    setState(() {
      recents = recentLsit;
    });
  }

  Future<void> _onRemovedAll() async {
    if (recents.isEmpty && recentLsit.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Recent lis is already Empty')));
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove(key);
      setState(() {
        recents = recentLsit = [];
      });
    }
  }
}
